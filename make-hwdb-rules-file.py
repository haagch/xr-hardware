#!/usr/bin/env python3
# Copyright 2019-2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
# Author: Ryan Pavlik <ryan.pavlik@collabora.com>
"""Make a .rules file for use with a hwdb file."""

from xrhardware.generate import generate_rules_file_contents

if __name__ == "__main__":

    print(generate_rules_file_contents(
        __file__,
        "70-xrhardware-hwdb.rules",
        '# This rules file must be used with the corresponding hwdb file'))
